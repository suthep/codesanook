﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CodeSanook.MicroBlog.Infrastructure;

namespace CodeSanook.MicroBlog.Model
{
    public class User : IAggregateRoot,IUtcCreateDate,IUtcLastUpdate
    {
        public virtual Guid Id { get; set; }
        public virtual string Email { get; set; }

        public virtual string Password { get; set; }
        public virtual bool IsActivated { get; set; }

        public virtual bool IsActive { get; set; }
        public virtual string CookieId { get; set; }

        public virtual IList<Role> Roles { get; set; }
        public virtual DateTime UtcCreateDate { get; set; }
        public virtual DateTime? UtcLastUpdate { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public User()
        {
            Roles = new List<Role>();
        }
    }
}

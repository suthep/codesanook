using CodeSanook.MicroBlog.Infrastructure;

namespace CodeSanook.MicroBlog.Model.RepositoryInterfaces
{
    public interface IArticleRepository : IRepository<Article, int>
    {
        Article GetByAlias(string alias);

        Tag GetTagByName(string tagName);
        void AddTag(Tag tag);
        int GetNumberOfArticles();


    }
}
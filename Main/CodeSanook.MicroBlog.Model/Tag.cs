﻿using System.Collections.Generic;

namespace CodeSanook.MicroBlog.Model
{
    public class Tag
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Article> Articles { get; set; }

        public Tag()
        {
            Articles = new List<Article>();
        }
    }
}
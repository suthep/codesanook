﻿using System;
using System.IO;
using System.Web.Mvc;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class FileController : Controller
    {

        public ActionResult GetFile(string relativePath)
        {
            //set false for already play music
            Session["isUserVisit"] = false;

            var filePath = Server.MapPath(relativePath);
            var fileStream = new FileStream(filePath, FileMode.Open);
           
            return File(fileStream,  GetContentType(filePath));
        }

        private static string GetContentType(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": 
                    return "Image/bmp";
                case ".gif": 
                    return "Image/gif";
                case ".jpg": 
                    return "Image/jpeg";
                case ".png": 
                    return "Image/png";
                case ".mp3":
                    return "audio/mpeg";
                case ".ogg":
                    return "application/ogg";
                case ".mp4":
                    return "video/mp4";
                case ".webm":
                    return "video/webm";
                case ".ogv":
                    return "video/ogg";
                default:
                    return string.Empty;
            }
          
        }

    }
}

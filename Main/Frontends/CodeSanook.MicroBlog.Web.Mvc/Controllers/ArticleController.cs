﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.Utility;
using log4net;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{

    //todo for update codesanook
    /*
     * port to entity framework code first 4.2
     * use my membership provider 
     * ck jquery edition
     * complete tag view page
     * remove tag widget and explan port 
     * ck editor easy to edit item
     */


    public class ArticleController : Controller
    {
        private const string TITLE_TEMPLATE = @"codesanook.com - 
                                                บทความ ASP.NET MVC, C#, MonoTouch, 
                                                Entity Framework, Unit Test ... {0}";
        private readonly string sessionKey = "captcha";
        private readonly int itemPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["articleListPerPage"]);
        public ArticleService ArticleService { get; set; }
        public ILog Log { get; set; }

        /// <summary>
        /// list all article
        /// </summary>
        /// <param name="id">page no</param>
        /// <returns>article list page for user</returns>
        public ActionResult Index(int? id)
        {
            var pageNo = id ?? 1;
            var response = ArticleService.GetArticleList(pageNo, itemPerPage);
            ViewBag.Pagination = new PaginationModel(
                pageNo,
                itemPerPage,
                response.Content.NumberOfArticles,
                "/article/index/");

            if (!response.IsSuccess)
            {
                return Content("error");
            }

            if (pageNo == 1)
            {
                ViewBag.Title = string.Format(TITLE_TEMPLATE, "หน้าแรก");
            }
            else
            {
                ViewBag.Title = string.Format(TITLE_TEMPLATE, "หน้า " + pageNo);
            }

            return View(response.Content.ArticleList);
        }

        /// <summary>
        /// diplay article details with comment
        /// </summary>
        /// <param name="articleId"> </param>
        /// <param name="alias">url friendly</param>
        /// <returns>article detials page</returns>
        public ActionResult Details(int articleId, string alias)
        {
            var response = ArticleService.GetArticleDetail(articleId);
            if (!response.IsSuccess)
            {
                ViewBag.Title = "codesanook.com - Article not found";
                return Content(ViewBag.Title);
            }

            ViewBag.Title = string.Format("codesanook.com - {0}", response.Content.Title);
            return View(response.Content);
        }

        [HttpPost]
        public ActionResult AddComment(Comment model, FormCollection form)
        {
            if (ValidCaptcha(model.CaptchaCode) && ModelState.IsValid)
            {
                try
                {
                    return Json(new
                    {
                        Success = true,
                        ErrorMessage = string.Empty,
                        model.UserName,
                        model.Details,
                        CreationDate = model.CreationDate.ToString("d-MMM-yy H:ss")
                    });

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            var errors = ModelState.Values.SelectMany(m => m.Errors.Select(e => e.ErrorMessage)).ToArray();

            string errorMesssage = string.Join("\n", errors);
            return Json(new { Success = false, ErrorMessage = errorMesssage });

        }

        /// <summary>
        /// show all article in this tag
        /// </summary>
        /// <param name="tagName">tag name to show</param>
        /// <param name="id">page no</param>
        /// <returns>page for article in tag</returns>
        public ActionResult Tag(string tagName, int? id)
        {
            throw new NotImplementedException();
            //int pageNo = id ?? 1;

            //var tag = db.Tags.SingleOrDefault(t => t.Name == tagName);
            //if (tag == null)
            //{
            //    return RedirectToAction("index");
            //}
            //var articles =
            //    tag.Articles.Where(a => a.IsPublish && DateTime.Now.CompareTo(a.ReleaseDate) > 0)
            //    .OrderByDescending(a => a.ReleaseDate)
            //    .TakeForPagination(pageNo, itemPerPage);

            //ViewBag.Pagination =
            //    new PaginationModel(pageNo, itemPerPage,
            //    tag.Articles.Count(), "/article/tag/" + Server.UrlEncode(tagName) + "/");

            //string title = "codesanook.com - บทความในหมวดหมู่ " +
            //                           tagName + " {0}";
            //if (pageNo == 1)
            //{
            //    ViewBag.Title = string.Format(title, "หน้าแรก");
            //}
            //else
            //{
            //    ViewBag.Title = string.Format(title, "หน้า " + pageNo);
            //}

            //return View("index", articles);

        }

        /// <summary>
        /// get all tag for tag auto complete
        /// </summary>
        /// <param name="q"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult GetTags(string q, int limit)
        {
            //string[] tags =
            //    db.Tags.Where(t => t.Name.StartsWith(q))
            //    .OrderBy(t => t.Name)
            //      .Take(limit)
            //      .Select(t => t.Name)
            //      .ToArray();

            //string tagList = string.Join("\n", tags);
            //return Content(tagList);
            throw new NotImplementedException();

        }

        public ActionResult RenderTagWidget()
        {
            //var tags = db.Tags.Where(t => t.Articles.Where(a => a.IsPublish).Count() > 0)
            //    .OrderBy(t => t.Name);
            //return PartialView("TagWidget", tags);
            //throw new NotImplementedException();
            return Content("");
        }



        [CodeSanookAuthorize(Roles = "admin")]
        public ActionResult Create()
        {
            var model = TempData["Model"] as Article;
            if (model == null)
            {
                model = new Article();
            }

            return View(model);
        }

        [CodeSanookAuthorize(Roles = "admin")]
        //[HttpPost, ValidateInput(false), ArticleImageUpload]
        public ActionResult Create(Article model)
        {
            var response = ArticleService.CreateArticle(model);

            if (response.IsSuccess)
            {
                return RedirectToAction("List");
            }

            ModelState.AddModelError("ErrorMessage", response.ErrorMessage);
            return View(model);
        }


        /// <summary>
        /// list all articles
        /// </summary>
        /// <returns></returns>
        [CodeSanookAuthorize(Roles = "admin")]
        public ActionResult List(int? id)
        {
            var pageNo = id ?? 1;
            var itemPerPage = 5;
            //todo need to order by release date
            var response = ArticleService.GetArticleList(pageNo, itemPerPage);
            ViewBag.Pagination = new PaginationModel(
                pageNo,
                itemPerPage,
                response.Content.NumberOfArticles,
                "/article/index/");

            ViewBag.Pagination = new PaginationModel(
                pageNo,
                itemPerPage,
                response.Content.NumberOfArticles,
                "/article/list/");

            return View(response.Content.ArticleList);

        }


        //[Authorize(Roles = "Admin")]
        [HttpPost, ValidateInput(false)]
        public ActionResult DeleteImage(string fileName, string returnUri, Article model)
        {
            try
            {

                var path = Server.MapPath("~/uploads/images/" + fileName);
                System.IO.File.Delete(path);
                model.PreviewImageUrl = string.Empty;
                TempData["model"] = model;
                return Redirect(returnUri);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }
            TempData["model"] = model;
            return Redirect(returnUri);

        }

        //[Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var article = TempData["model"] as Article;

            if (article != null)
            {
                return View(article);
            }

            return RedirectToAction("List");
        }

        // [Authorize(Roles = "Admin")]
        //[HttpPost, ValidateInput(false), ArticleImageUpload]
        public ActionResult Edit(int id, Article article)
        {
            ArticleService.EditArticle(id, article);
            return View(article);
        }

        /// <summary>
        /// delete article
        /// </summary>
        /// <param name="id">Guid of article to be deleted</param>
        /// <returns>list view without deleted article</returns>
        [CodeSanookAuthorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            ArticleService.DeleteArticle(id);
            return RedirectToAction("List");

        }

        [CodeSanookAuthorize(Roles = "admin")]
        public ActionResult UploadImage(int ckEditorFuncNum)
        {
            //query string {type=Images&CKEditor=Details&CKEditorFuncNum=2&langCode=en}
            var request = Request.QueryString;
            var imageHelper = new ImageHelper();
            const int imageWidth = 700;
            var uploadedImageUrls = imageHelper.Upload(imageWidth, imageWidth);

            var url = string.Empty;
            if (uploadedImageUrls != null && uploadedImageUrls.Any())
            {
                url = uploadedImageUrls[0];
            }

            return Content(string.Format("<script>window.parent.CKEDITOR.tools.callFunction( {0}, '{1}','');</script>", ckEditorFuncNum, url));

        }

        /// <summary>
        /// toggle publish and unpublish article
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [CodeSanookAuthorize(Roles = "admin")]
        public ActionResult Publish(int id)
        {
            ArticleService.TogglePublishArticle(id);
            return RedirectToAction("List");
        }

        /// <summary>
        /// render captcha image
        /// </summary>
        /// <returns>captcha image</returns>
        public ActionResult RenderCaptcha()
        {
            //var result = CaptchaHelper.GetCaptcha();
            //if (Session[sessionKey] == null)
            //{
            //    //create session 
            //    Session.Add(sessionKey, result.Solution);
            //}
            ////use exist one
            //Session[sessionKey] = result.Solution;

            //return new FileContentResult(result.Image, result.ContentType);
            throw new NotImplementedException();
        }

        /// <summary>
        /// valid captcha user type is correct
        /// </summary>
        /// <param name="captchaCode">captcha from user</param>
        /// <returns>true if correct</returns>
        public bool ValidCaptcha(string captchaCode)
        {
            if (string.IsNullOrEmpty(captchaCode) || Session[sessionKey] == null)
            {
                //ModelState.AddModelError("CaptchaCode", "กรุณากรอกตัวอักษรที่ปรากฏในรูปภาพ");
                return false;
            }

            var code = (string)Session[sessionKey];

            bool result = string.Compare(code.ToLower(), captchaCode.ToLower()) == 0;

            if (!result)
            {
                ModelState.AddModelError("CaptchaCode", "กรุณากรอกตัวอักษรที่ปรากฏในรูปให้ถูกต้อง");
            }

            return result;
        }

        public ActionResult Like()
        {

            // var fb = new FacebookApp("appid", "appsecret");







            //var client = new FacebookWebClient();



            //var args = new Dictionary<string, object>();
            //args["message"] = "test";
            //args["caption"] = "This is caption!";
            //args["description"] = "This is description!";
            //args["name"] = "This is name!";
            //args["picture"] = "http://itsmagic.files.wordpress.com/2009/11/stewie1.jpg";
            //args["link"] = "http://localhost:1080";



            ////JsonObject result = (JsonObject)client.Post("/me/feed", args);

            //dynamic me = client.Get("me");

            //var result = client.Post(string.Format("/{0}/links", me.id), args);


            //return new JsonResult
            //{
            //    Data = (result as dynamic).id,
            //    JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //};
            throw new NotImplementedException();
        }

        public ActionResult LikeCount()
        {

            //var query = "SELECT user_id FROM like WHERE object_id=\"1064101575_2529176783108\"";
            //var client = new FacebookWebClient();
            //var result = client.Query(query);
            //return Content("");
            throw new NotImplementedException();
        }



    }


}

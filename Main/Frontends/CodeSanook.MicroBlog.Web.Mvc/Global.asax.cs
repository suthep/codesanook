﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Repositories.NH;
using CodeSanook.MicroBlog.Repositories.NH.Repositories;
using CodeSanook.MicroBlog.Web.Mvc.App_Start;
using log4net;
using log4net.Config;

namespace CodeSanook.MicroBlog.Web.Mvc
{

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            SetupAutofac();
        }

        private void SetupAutofac()
        {

            var builder = new Autofac.ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly)
                .PropertiesAutowired();

            var dataAccess = Assembly.GetAssembly(typeof(ArticleRepository));
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();


            var service = Assembly.GetAssembly(typeof(ArticleService));
            builder.RegisterAssemblyTypes(service)
                .Where(t => t.Name.EndsWith("Service"))
                .PropertiesAutowired()
                .InstancePerHttpRequest();

            //unit of work
            var unitOfWorkFactory = new NHUnitOfWorkFactory();
            builder.RegisterInstance(unitOfWorkFactory).As<IUnitOfWorkFactory>();

            var log = LogManager.GetLogger("CodeSanook");
            builder.RegisterInstance(log).As<ILog>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}
﻿(function($) {


    $.fn.limitChars = function(limit) {

        this.each(function() {

            //insert new element to show infor
            $(this).after("<div class='limitCharsInfo'></div");

            $(this).keyup(function() {

                var text = $(this).val();

                var textlength = text.length;

                if (textlength > limit) {

//.first()

                $(this).next()
                    .html('คุณไม่สามารถพิมพ์ได้มากกว่า ' + limit + ' ตัวอักษร!');
                    $(this).val(text.substr(0, limit));
                    return false;
                }
                else {
                    $(this).next()
                    .html('คุณสามารถพิมพ์ได้อีก ' + (limit - textlength) +
                ' ตัวอักษร จากทั้งหมด ' + limit + ' ตัวอักษร');
                    return true;
                }

            }); //end key up function






        }); //end each function



        function limitChars(textid, limit, infodiv) {
            var text = $('#' + textid).val();
            var textlength = text.length;
            if (textlength > limit) {
                $('#' + infodiv).html('You cannot write more then ' + limit + ' characters!');
                $('#' + textid).val(text.substr(0, limit));
                return false;
            }
            else {
                $('#' + infodiv).html('You have ' + (limit - textlength) +
                ' characters left from ' + limit + ' characters');
                return true;
            }
        } //end limit chars sub function



    } //end limit char jquery function



})(jQuery)
﻿using System.Collections;
using System.Collections.Generic;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Persister.Entity;
using log4net;
using System.Linq;

namespace CodeSanook.MicroBlog.Repositories.NH
{
    public abstract class RepositoryBase<T, K> : IRepository<T, K> where T : class, IAggregateRoot
    {
        protected ILog _log = LogManager.GetLogger("CodeSanook.MicroBlog.Repositories.NH");

        public void Add(T entity)
        {
            SessionFactory.GetCurrentSession().Save(entity);
        }

        public void Remove(T entity)
        {
            SessionFactory.GetCurrentSession().Delete(entity);
        }


        public T FindById(K id)
        {

            return SessionFactory.GetCurrentSession().Get<T>(id);
        }

        public IList<T> FindAll()
        {
            var session = SessionFactory.GetCurrentSession();
            var criteria = session.CreateCriteria(typeof(T));

            return criteria.List<T>();
        }

        public IList<T> FindAll(int index, int count)
        {
            var session = SessionFactory.GetCurrentSession();
            var criteria = session.CreateCriteria<T>();
            criteria.SetFirstResult(index);
            criteria.SetMaxResults(count);

            return criteria.List<T>();
        }

        public T1 GetBy<T1>(params object[] keyValue) where T1 : class
        {
            //A brief explanation:
            //GetProperties: all the mapped properties for a given entity;
            //GetComponents: given an entity type, returns all the properties that are mapped as components;
            //GetUserTypes: all the properties that are mapped as custom user types;
            //GetIdentifier: the property that is mapped as the identifier;
            //GetAssociations: properties mapped as one to one or many to one, that is, that point to another entity;
            //GetCollections: properties mapped as one to many, that is, collections, of any type;
            //GetTableName: the table or view name where the entity is stored;
            //GetIdentifierColumnNames: the names of the column(s) that compose the primary key for the table;
            //GetColumnName: the table or view column that store a given property.

            var sessionFactory = SessionFactory.GetSessionFactory();
            var classMetadata = sessionFactory.GetClassMetadata(typeof(T1));

            // Gets the entity's persister
            var persister = (AbstractEntityPersister)classMetadata;

            var session = SessionFactory.GetCurrentSession();
            var criteria = session.CreateCriteria<T1>();


            var primaryKeys = persister.IdentifierColumnNames.OrderBy(k => k).ToArray();//order by name asc
            for (var i = 0; i < primaryKeys.Length; i++)
            {
                var primaryKeyName = primaryKeys[i];
                var primaryKeyValue = keyValue[i];
                _log.DebugFormat("primaryKey name: {0} value: {1}", primaryKeyName, primaryKeyValue);

                criteria.Add(Restrictions.Eq(primaryKeyName, primaryKeyValue));
            }

            return criteria.UniqueResult<T1>();
        }

        public T1 GetBy<T1>(IDictionary<string, object> keyValues) where T1 : class
        {
            var session = SessionFactory.GetCurrentSession();
            var criteria = session.CreateCriteria<T1>();
            foreach (var key in keyValues.Keys)
            {
                var keyValue = keyValues[key];
                criteria.Add(Restrictions.Eq(key, keyValue));
            }

            return criteria.UniqueResult<T1>();
        }
    }
}

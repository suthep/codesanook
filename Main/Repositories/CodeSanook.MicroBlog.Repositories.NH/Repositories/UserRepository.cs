﻿using System;
using System.Collections.Generic;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;

namespace CodeSanook.MicroBlog.Repositories.NH.Repositories
{
    public class UserRepository : RepositoryBase<User, Guid>, IUserRepository
    {
        public User GetByCookieId(string cookieId)
        {
            throw new NotImplementedException();
        }

        public int GetRowCountUserWithEmailAndPassword(string email, string password)
        {
            throw new NotImplementedException();
        }

        public User GetByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public void AddRole(Role role)
        {
            throw new NotImplementedException();
        }

        public Role GetRoleByName(string roleName)
        {
            throw new NotImplementedException();
        }

        public IList<Role> GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public void RemoveRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public IList<User> GetAllUserInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public IList<Role> GetAllRoleForUser(int userId)
        {
            throw new NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            throw new NotImplementedException();
        }
    }
}
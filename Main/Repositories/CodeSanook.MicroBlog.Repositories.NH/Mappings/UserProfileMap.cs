using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class UserProfileMap : ClassMap<UserProfile>
    {
        public UserProfileMap()
        {
            Table("UserProfiles");

            Id(x => x.Id).GeneratedBy.Foreign("User");
            Map(x => x.DisplayName).Length(255);
            HasOne(x => x.User).Constrained().ForeignKey("none");
        }
    }
}
﻿using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class TbMasCancellationInfoMap : ClassMap<TbMasCancellationInfo>
    {
        public TbMasCancellationInfoMap()
        {
            CompositeId(x => x.Key)
                .KeyProperty(x => x.CancelId, "CancellationId")
                .KeyProperty(x => x.CultureId, "CultureId");

            References(x => x.Cancellation, "CancellationId")
                .ForeignKey("none")
                .Not.Insert();
            Map(x => x.CancellationReason);
        }

    }
}
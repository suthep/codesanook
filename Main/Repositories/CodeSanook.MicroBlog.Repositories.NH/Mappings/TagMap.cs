﻿using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class TagMap:ClassMap<Tag>
    {
        public TagMap()
        {
            Table("Tags");

            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(255).Not.Nullable();
            HasManyToMany(x => x.Articles)
                .ForeignKeyConstraintNames("none", "none")
                .ParentKeyColumn("TagId")
                .ChildKeyColumn("ArticleId");
        }
    }
}
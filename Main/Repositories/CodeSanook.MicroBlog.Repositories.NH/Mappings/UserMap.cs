﻿using System;
using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("Users");
            Id(x => x.Id).GeneratedBy.GuidComb();
            HasOne(x => x.UserProfile).Cascade.All().LazyLoad();

            Map(x => x.Email).Not.Nullable().Length(255);
            Map(x => x.Password).Not.Nullable().Length(255);
            Map(x => x.IsActivated).Not.Nullable().Default("0");
            Map(x => x.IsActive).Not.Nullable().Default("0");
            Map(x => x.CookieId).Nullable().Length(255);

            Map(x => x.UtcCreateDate).Not.Nullable();
            Map(x => x.UtcLastUpdate);

            HasManyToMany(x => x.Roles).ForeignKeyConstraintNames("none", "none")
                .ParentKeyColumn("UserId")
                .ChildKeyColumn("RoleId");

        }
    }
}

/*
Primary key association

public SettingMap()
{
    Id(x => x.Id).GeneratedBy.Guid();
    HasOne(x => x.Student).Cascade.All();
}

public StudentMap()
{
    Id(x => x.Id).GeneratedBy.Foreign("Setting");
    HasOne(x => x.Setting).Constrained();
}

and a new setting instance should be stored:

        var setting = new Setting();

        setting.Student = new Student();
        setting.Student.Name = "student1";
        setting.Student.Setting = setting;
        setting.Name = "setting1";

        session.Save(setting);

Foreign key association

public SettingMap()
{
    Id(x => x.Id).GeneratedBy.Guid();
    References(x => x.Student).Unique().Cascade.All();
}

public StudentMap()
{
    Id(x => x.Id).GeneratedBy.Guid();
    HasOne(x => x.Setting).Cascade.All().PropertyRef("Student");
}

*/
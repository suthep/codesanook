﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    /// <summary>
    /// model for Article
    /// </summary>


 //http://stackoverflow.com/questions/11286892/fluent-nhibernate-composite-key-mapping   

//short answer: you can't share columns for multiple references

//long answer: NHibernate treats every reference independent of each other but does eliminate duplicate columns in insert statements, hence the references try to access columns which are not present anymore. it does so because if the shared column differs between the to references in the object model, it can't decide which one is correct.

//If you can change the database schema and make the ids unique then ignore the year all together in the ids and references.

//Update:

//you can simplify some of the mappings

//CompositeId()
//    .KeyProperty(x => x.Year, set => {
//        set.ColumnName("Year");
//        set.Access.Property(); } )
//    .KeyProperty(x => x.CommissionLevelID, set => {
//        set.ColumnName("CommissionLevelID");
//        set.Length(10);
//        set.Access.Property(); } );

//// to
//CompositeId()
//    .KeyProperty(x => x.Year)  // columnname is equal propertyname by default
//    .KeyProperty(x => x.CommissionLevelID, set => set.Length(10).Access.Property());  // property is default access and can also be left out


//.SqlType("VARCHAR2").Length(10)
//// to
//.Length(10) or .SqlType("VARCHAR2")
//// because length is ignored when sqltype is specified

//share|improve this answer
	
//edited yesterday
//w00ngy
//9619
	
//answered Jul 2 '12 at 6:01
//Firo
//20.6k21642
	
   	 
	
//Thanks a lot. Unfortunately I cannot change the db_schema. DBA doesn't allow us to do it. Can I ask where did you read those knowledge about NHibernate? Is there a documentation somewhere? –  user1494907 Jul 2 '12 at 23:41
   	 
	
//most of it is painful experience with legacy dbs. I discovered the causes by googling, reading NH source, trial and error. Everytime you encounter such a problem you have to make compromises. Either map it differently (eg ignore primary/foreign keys in db if parts are unique), bleed private properties into the domainmodel, implement nhibernate hooks to get around the default handling of NHibernate. –  Firo Jul 3 '12 at 5:40 
            //Id(x => x.Id).GeneratedBy.Identity();
    public class ArticleMap : ClassMap<Article>
    {
        public ArticleMap()
        {
            Table("Articles");

            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Title).Not.Nullable().Length(255);

            References(x => x.Author).ForeignKey("none").Column("AuthorId");//.Not.Nullable();
            Map(x => x.Alias).Length(255);//.Not.Nullable();
            Map(x => x.Details).Length(10000);//.Not.Nullable();
            Map(x => x.Abstract).Length(1000);//.Not.Nullable();
            Map(x => x.PreviewImageUrl).Length(255);//.Not.Nullable();
            Map(x => x.UtcReleaseDate);//.Not.Nullable();
            Map(x => x.IsPublish);//.Not.Nullable().Default("0");
            Map(x => x.Hits);//.Not.Nullable().Default("0");
            Map(x => x.UtcCreateDate);//.Not.Nullable();
            Map(x => x.UtcLastUpdate);

            HasMany(x => x.Comments).ForeignKeyConstraintName("none").KeyColumn("ArticleId");

            HasManyToMany(x => x.Tags)
                .ForeignKeyConstraintNames("none", "none")
                .ParentKeyColumn("ArticleId")
                .ChildKeyColumn("TagId");
        }
    }
}

﻿using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class RoleMap:ClassMap<Role>
    {
        public RoleMap()
        {
            Table("Role");

            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name);
            HasManyToMany(x => x.Users).ForeignKeyConstraintNames("none", "none")
                .ParentKeyColumn("RoleId")
                .ChildKeyColumn("UserId");
        }

    }
}

﻿using System;
using CodeSanook.MicroBlog.Infrastructure;
using NHibernate.Event;
using NHibernate.Persister.Entity;
using log4net;

namespace CodeSanook.MicroBlog.Repositories.NH
{
    /// <summary>
    /// help to set CreateDate and LastUpdate Value for Model Class
    /// </summary>
    public class AuditEventListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        private ILog _log = LogManager.GetLogger("AuditEventListener");

        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            var audit = @event.Entity as IUtcLastUpdate;
            if (audit == null)
                return false;

            var utcNow = DateTime.UtcNow;
            //log.DebugFormat("localNow = {0}, audit type {1}", localNow, audit.GetType());            

            Set(@event.Persister, @event.State, "UtcLastUpdate",utcNow);
            audit.UtcLastUpdate =utcNow;
            return false;
        }
       
        public bool OnPreInsert(PreInsertEvent @event)
        {
            var audit = @event.Entity as IUtcCreateDate;
            if (audit == null)
                return false;

            var utcNow = DateTime.UtcNow;
            //log.DebugFormat("localNow = {0}, audit type {1}", localNow, audit.GetType());            

            Set(@event.Persister, @event.State, "UtcCreateDate",utcNow);
            audit.UtcCreateDate =utcNow;
            return false;
        }

        private void Set(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            var index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1)
                return;
            state[index] = value;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web;

namespace CodeSanook.Utility
{
    public class ImageHelper
    {
        private const string urlPath = "/uploads/images";


        public string[] Upload(int maxWidth, int maxHeight)
        {
            var directoryPath = HttpContext.Current.Server.MapPath("~" + urlPath);

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);

            }

            var fileNames = HttpContext.Current.Request.Files.AllKeys;
          
            var uplodedImageUrls = new List<string>();

            foreach (var fileName in fileNames)
            {
              
              HttpPostedFile file =  HttpContext.Current.Request.Files[fileName];

                if (file.ContentLength != 0)
                {

                 
                 

                        var image = Image.FromStream(file.InputStream);
                        var bmp = image.Redimension(maxWidth,maxHeight);

                        string uploadedImageFileName = Guid.NewGuid() +
                                                       Path.GetExtension(file.FileName);
                        var path = Path
                            .Combine(directoryPath, uploadedImageFileName);

                        bmp.Save(path);

                        uplodedImageUrls.Add(urlPath + "/" + uploadedImageFileName);

                    }
         


                }


            return uplodedImageUrls.ToArray();
        
        }
    }
}
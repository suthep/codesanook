﻿using XCaptcha;

namespace CodeSanook.Utility.Captcha
{
   public class CaptchaHelper
    {

       public static CaptchaResult GetCaptcha()
       {
           var builder = new XCaptcha.ImageBuilder(4, new CustomCanvas(), new CustomTextStyle(), new CustomDistort(), new CustomNoise());
           //Uncomment for default version
           //var builder = new XCaptcha.ImageBuilder();
          
           CaptchaResult result = builder.Create();
           
           return result;

       }
    }
}

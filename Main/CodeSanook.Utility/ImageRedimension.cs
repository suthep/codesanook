﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace CodeSanook.Utility
{
   public static class ImageRedimension
    {

       public static Bitmap Redimension(this Image image , 
           int maxWidth,int maxHeight)
       {
         

                    int width = image.Width;
                    int height = image.Height;

                    //create full image 
                    if (image.Width > maxWidth || image.Height > maxHeight)
                    {


                        if ((double)image.Width / (double)image.Height > (double)maxWidth / (double)maxHeight)
                        {
                            width = maxWidth;
                            height = (int)(image.Height * maxWidth / image.Width);
                        }
                        else
                        {
                            width = (int)(image.Width * maxHeight / image.Height);
                            height = maxHeight;
                        }
                    }

                    Bitmap bmp = new Bitmap(width, height);

                    Graphics g = Graphics.FromImage(bmp);
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.DrawImage(image, 0, 0, width, height);
                    return bmp;
    }

}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace CodeSanook.Utility
{
    public static class CustomHelper
    {

        public static string RatingFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression)
        {
            /*
            MemberExpression body = expression.Body as MemberExpression;

            string tagName = body.Member.Name;
           int[] imageIds = expression.Compile()(helper.ViewData.Model);
             */


            TagBuilder ratingContainer = new TagBuilder("div");
            ratingContainer.Attributes["class"] = "rating";
            TagBuilder ratingItem = null;

            for (int i = 1; i <= 5; i++)
            {
                ratingItem = new TagBuilder("input");
                ratingItem.MergeAttributes(new RouteValueDictionary(new
                {
                    type = "image",
                    src = "../../Content/Images/EmptyStar.png",
                    value = i.ToString()

                }));
                ratingContainer.InnerHtml += ratingItem.ToString(TagRenderMode.SelfClosing);
            }


            TagBuilder updateValueItem = new TagBuilder("input");
            updateValueItem.MergeAttributes(new RouteValueDictionary(new
            {
                type = "hidden",
                value = expression.Compile()(helper.ViewData.Model).ToString()
            }));

            ratingContainer.InnerHtml += updateValueItem.ToString(TagRenderMode.SelfClosing);
            return ratingContainer.ToString();
        }


        /// <summary>
        /// creat star rating
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="modelId">id of model to be rated</param>
        /// <param name="actionName">action method name that perform rating and update database</param>
        /// <param name="controllerName">controller name where contain action method</param>
        /// <returns></returns>
        public static string Rating(this HtmlHelper helper,
         int modelId, string actionName, string controllerName)
        {


            TagBuilder ratingContainer = new TagBuilder("div");
            ratingContainer.Attributes["class"] = "rating";
            TagBuilder ratingItem = null;

            for (int i = 1; i <= 5; i++)
            {
                ratingItem = new TagBuilder("input");
                ratingItem.MergeAttributes(new RouteValueDictionary(new
                {
                    type = "image",
                    src = "../../Content/Images/EmptyStar.png",
                    value = i.ToString()

                }));
                ratingContainer.InnerHtml += ratingItem.ToString(TagRenderMode.SelfClosing);
            }

            TagBuilder updateValueItem = new TagBuilder("input");
            updateValueItem.MergeAttributes(new RouteValueDictionary(new
            {
                type = "hidden",
                value = modelId.ToString()
            }));

            TagBuilder action = new TagBuilder("input");
            action.MergeAttributes(new RouteValueDictionary(new
            {
                type = "hidden",
                value = actionName,
                @class = "actionName"
            }));

            TagBuilder controller = new TagBuilder("input");
            controller.MergeAttributes(new RouteValueDictionary(new
            {
                type = "hidden",
                value = controllerName,
                @class = "controllerName"
            }));

            ratingContainer.InnerHtml += updateValueItem.ToString(TagRenderMode.SelfClosing) +
                action.ToString(TagRenderMode.SelfClosing) + controller.ToString(TagRenderMode.SelfClosing);
            return ratingContainer.ToString();
        }
        /// <summary>
        /// create pagination
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="paginationModel.PageNo">page No.</param>
        /// <param name="pageSize">size of items to show per page</param>
        /// <param name="modelSize">tatal rows of model table</param>
        /// <param name="action">action method name</param>
        /// <param name="controller">controller name that has action method to populate pagination</param>
        /// <returns>pagination</returns>
        public static string Pagination(this HtmlHelper helper, PaginationModel paginationModel)
        {
            if (paginationModel == null) return string.Empty;


            int adjacents = 3;

            /* 
               First get total number of rows in data table. 
               If you have a WHERE clause in your query, make sure you mirror it here.
            */


            /* Setup vars for query. */
            string actionMethod = paginationModel.ActionURL; // /controlller/action/
            // int pageSize = 2;//how many items to show per page
            int start = 0;
            if (paginationModel.PageNo > 0)
                start = (paginationModel.PageNo - 1) * paginationModel.ItemPerPage; 			//first item to display on this page
            //if no page var is given, set start to 0

            /* Get data. */
            //$sql = "SELECT column_name FROM $tbl_name LIMIT start, take";
            //$result = mysql_query($sql);

            /* Setup page vars for display. */
            if (paginationModel.PageNo == 0) paginationModel.PageNo = 1;					//if no page var is given, default to 1.
            int prev = paginationModel.PageNo - 1;							//previous page is page - 1
            int next = paginationModel.PageNo + 1;	//next page is page + 1

            int lastPage = (int)Math.Ceiling((decimal)paginationModel.ModelSize / (decimal)paginationModel.ItemPerPage);		//lastPage is = total pages / items per page, rounded up.
            int lpm1 = lastPage - 1;						//last page minus 1

            /* 
                Now we apply our rules and draw the pagination object. 
                We're actually saving the code to a variable in case we want to draw it more than once.
            */


            string pagination = "";

            if (lastPage > 1)
            {
                pagination += "<div class=\"pagination\">";
                //previous button
                if (paginationModel.PageNo > 1)
                    pagination += "<a href=\"" + actionMethod + prev + "\"><< ก่อนหน้า</a>";
                else
                    pagination += "<span class=\"disabled\"><< ก่อนหน้า</span>";

                //pages	
                if (lastPage < 7 + (adjacents * 2))	//not enough pages no need to hide them
                {
                    int counter;
                    for (counter = 1; counter <= lastPage; counter++)
                    {
                        if (counter == paginationModel.PageNo)
                            pagination += "<span class=\"current\">" + counter + "</span>";
                        else
                            pagination += "<a href=\"" + actionMethod + counter + "\">" + counter + "</a>";
                    }

                    //implement next page here
                    if (paginationModel.PageNo < counter - 1)
                        pagination += "<a href=\"" + actionMethod + next + "\">ถัดไป >></a>";
                    else
                        pagination += "<span class=\"disabled\">ถัดไป >></span>";



                }
                else if (lastPage > 5 + (adjacents * 2))	//enough pages to hide some
                {
                    //close to beginning; only hide later pages
                    if (paginationModel.PageNo < 1 + (adjacents * 2)) //start hide if page == 7
                    {
                        int counter;
                        for (counter = 1; counter < 4 + (adjacents * 2); counter++)
                        {
                            if (counter == paginationModel.PageNo)
                                pagination += "<span class=\"current\">" + counter + "</span>";
                            else
                                pagination += "<a href=\"" + actionMethod + counter + "\">" + counter + "</a>";
                        }
                        pagination += "...";
                        pagination += "<a href=\"" + actionMethod + lpm1 + "\">" + lpm1 + "</a>";
                        pagination += "<a href=\"" + actionMethod + lastPage + "\">" + lastPage + "</a>";

                        //implement next button;
                        if (paginationModel.PageNo < counter - 1)
                            pagination += "<a href=\"" + actionMethod + next + "\">ถัดไป >></a>";
                        else
                            pagination += "<span class=\"disabled\">ถัดไป >></span>";


                    }
                    //in middle; hide some front and some back
                    //ex page == 7
                    //hide after page 2 and befor last 2 page
                    else if (lastPage - (adjacents * 2) > paginationModel.PageNo && paginationModel.PageNo > (adjacents * 2))//start hide if page == 7
                    {
                        pagination += "<a href=\"" + actionMethod + 1 + "\">1</a>";
                        pagination += "<a href=\"" + actionMethod + 2 + "\">2</a>";
                        pagination += "...";
                        int counter;
                        for (counter = paginationModel.PageNo - adjacents; counter <= paginationModel.PageNo + adjacents; counter++)
                        {
                            if (counter == paginationModel.PageNo)
                                pagination += "<span class=\"current\">" + counter + "</span>";
                            else
                                pagination += "<a href=\"" + actionMethod + counter + "\">" + counter + "</a>";
                        }
                        pagination += "...";
                        pagination += "<a href=\" " + actionMethod + lpm1 + "\">" + lpm1 + "</a>";
                        pagination += "<a href=\"" + actionMethod + lastPage + "\">" + lastPage + "</a>";
                        //todo implement last button
                        if (paginationModel.PageNo < counter - 1)
                            pagination += "<a href=\"" + actionMethod + next + "\">ถัดไป >></a>";
                        else
                            pagination += "<span class=\"disabled\">ถัดไป >></span>";
                    }
                    //close to end; only hide early pages
                    else
                    {
                        pagination += "<a href=\"" + actionMethod + 1 + "\">1</a>";
                        pagination += "<a href=\"" + actionMethod + 2 + "\">2</a>";
                        pagination += "...";
                        int counter;
                        for (counter = lastPage - (2 + (adjacents * 2)); counter <= lastPage; counter++)
                        {
                            if (counter == paginationModel.PageNo)
                                pagination += "<span class=\"current\">" + counter + "</span>";
                            else
                                pagination += "<a href=\"" + actionMethod + counter + "\">" + counter + "</a>";
                        }


                        //next button
                        if (paginationModel.PageNo < counter - 1)
                            pagination += "<a href=\"" + actionMethod + next + "\">ถัดไป >></a>";
                        else
                            pagination += "<span class=\"disabled\">ถัดไป >></span>";

                    }
                }

                pagination += "</div>\n";
            }

            return pagination;

        }

        public static string DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression)
        {
            //<input class="datePicker" type="text" id="date" />
            MemberExpression body = expression.Body as MemberExpression;

            string tagName = body.Member.Name;

            TagBuilder datePicker = new TagBuilder("input");
            datePicker.MergeAttributes(new RouteValueDictionary(new
            {
                type = "text",
                @class = "datePicker",
                name = tagName
            }));

            return datePicker.ToString(TagRenderMode.SelfClosing);
        }
        /// <summary>
        /// crate datePicker
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="modelPropertyName">name of model property</param>
        /// <param name="modelProperyValue">value of model property</param>
        /// <returns></returns>
        public static string DatePicker(this HtmlHelper helper, string modelPropertyName,
            string modelProperyValue)
        {
            //<input class="datePicker" type="text" id="date" />


            string tagName = modelPropertyName;

            TagBuilder datePicker = new TagBuilder("input");
            datePicker.MergeAttributes(new RouteValueDictionary(new
            {
                type = "text",
                @class = "datePicker",
                name = tagName,
                value = helper.ViewData.Model != null ? modelProperyValue : ""
            }));

            return datePicker.ToString(TagRenderMode.SelfClosing);
        }





        /// <summary>
        /// Render Label Tag
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <param name="innerText"></param>
        /// <returns></returns>

        public static string LabelFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string innerText)
        {
            return LabelFor(helper, expression, innerText, null);
        }

        /// <summary>
        /// Render label tag
        /// </summary>
        /// <param name="target">id value of input target</param>
        /// <param name="text">text to show on label</param>
        /// <returns>label tag</returns>
        public static string LabelFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string innerText, object htmlAttributes)
        {
            var builder = new TagBuilder("label");
            // builder.Attributes["id"] =
            var body = expression.Body as MemberExpression;
            builder.Attributes["for"] = body.Member.Name;
            builder.InnerHtml = innerText;
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            return builder.ToString();
        }


        /// <summary>
        /// Create CheckBox List
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="name"></param>
        /// <param name="selectListItem"></param>
        /// <returns></returns>
        public static string CheckBoxList(this HtmlHelper helper, string name, IEnumerable<SelectListItem> selectListItem)
        {
            var ul = new TagBuilder("ul");
            ul.MergeAttribute("id", name);
            ul.MergeAttribute("style", "padding:0");
            foreach (var item in selectListItem)
            {

                var li = new TagBuilder("li");

                var input = new TagBuilder("input");
                var label = new TagBuilder("label");



                input.MergeAttribute("type", "checkbox");
                input.MergeAttribute("id", "subcategoryId" + item.Value);
                input.MergeAttribute("value", item.Value);
                if (item.Selected)
                {
                    input.MergeAttribute("checked", "checked");
                }


                //  input.MergeAttribute("checked", "checked");


                label.MergeAttribute("for", "subcategoryId" + item.Value);
                label.InnerHtml = item.Text;

                if (item.Value != "0")
                {

                    li.InnerHtml = input.ToString() + label.ToString();
                }
                else
                {

                    li.InnerHtml = label.ToString();
                }

                li.MergeAttribute("style", "list-style:none");
                ul.InnerHtml += li.ToString();
            }


            return ul.ToString();
        }








        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="array"></param>
        /// <param name="name"></param>
        /// <returns></returns>

        public static string DeclareJSArray<T>(this HtmlHelper helper, IEnumerable<T> array, string name)
        {
            var tagBuilder = new TagBuilder("script");
            tagBuilder.MergeAttribute("type", "text/javascript");

            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            int counter = 0;
            foreach (var s in array)
            {

                sb.Append("\"");
                sb.Append(s.ToString());
                sb.Append("\"");
                counter++;
                if (counter < array.Count())
                {
                    sb.Append(", ");
                }
            }
            sb.Append("];");
            tagBuilder.InnerHtml = "var " + name + " = " + sb.ToString();


            return tagBuilder.ToString();
        }

        //todo improve image uploaderfor
        public static string ImageUploaderFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            string deleteImageAction, string deleteImageController, object htmlAttributes)
        {
            //get tag name from expression 
            MemberExpression memberExpression = expression.Body as MemberExpression;
            // string tagName = memberExpression.Member.Name;
            // var model = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).Model;
            string tagName = ExpressionHelper.GetExpressionText(expression);

            TagBuilder imageUploaderContainer = new TagBuilder("div");
            imageUploaderContainer.Attributes["class"] = "imageUploader";

            //check if model is null
            if (helper.ViewData.Model != null)
            {
                TProperty imageName = expression.Compile()(helper.ViewData.Model);





                if (imageName != null && imageName.ToString() != string.Empty)
                {



                    TagBuilder image = new TagBuilder("img");
                    image.MergeAttributes(new RouteValueDictionary(new
                    {
                        src = imageName.ToString(),
                        @class = "imageBlock"
                    }));
                    image.MergeAttributes(new RouteValueDictionary(htmlAttributes));


                    TagBuilder deleteButton = new TagBuilder("a");
                    deleteButton.MergeAttributes(new RouteValueDictionary(new
                    {
                        href = "/" + deleteImageController + "/" + deleteImageAction + "/",
                        @class = "deleteButton"
                    }));
                    deleteButton.InnerHtml = "ลบรูป";

                    TagBuilder imageSrc = new TagBuilder("input");
                    imageSrc.MergeAttributes(new RouteValueDictionary(new
                    {
                        type = "hidden",
                        name = tagName,
                        value = imageName.ToString(),
                    }));

                    TagBuilder clear = new TagBuilder("div");
                    clear.Attributes["style"] = "clear:both";

                    imageUploaderContainer.InnerHtml = image.ToString(TagRenderMode.SelfClosing) +
                    deleteButton.ToString() + imageSrc.ToString(TagRenderMode.SelfClosing) +
                    clear.ToString();
                    return imageUploaderContainer.ToString();

                }

                TagBuilder fileInputField = new TagBuilder("div");
                fileInputField.Attributes["class"] = "fileInputField";
                TagBuilder fileInput = new TagBuilder("input");
                fileInput.Attributes["name"] = tagName;
                fileInput.MergeAttributes(new RouteValueDictionary(new { type = "file", @class = "fileInputField" }));
                fileInputField.InnerHtml = fileInput.ToString(TagRenderMode.SelfClosing);
                imageUploaderContainer.InnerHtml += fileInputField.ToString();
                return imageUploaderContainer.ToString();

            }
            else //show input file when model property is null
            {


                TagBuilder fileInput = new TagBuilder("input");
                fileInput.Attributes["name"] = tagName;
                fileInput.MergeAttributes(new RouteValueDictionary(new { type = "file" }));
                imageUploaderContainer.InnerHtml += fileInput.ToString();

                return imageUploaderContainer.ToString();

            }

        }

        /*
        public static string ImageUploaderFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, int[]>> expression,
            int numberOfImages, string imageSourceAction, string imageSourceController, string deleteImageAction, string deleteImageController)
        {
            //model => model.detail;

            MemberExpression body = expression.Body as MemberExpression;

            string tagName = body.Member.Name;
            if (helper.ViewData.Model != null)
            {

                int[] imageIds = expression.Compile()(helper.ViewData.Model);




                TagBuilder imageUploaderContainer = new TagBuilder("div");
                imageUploaderContainer.Attributes["class"] = "imageUploader";
                TagBuilder imageContainer;
           
                TagBuilder image;
                TagBuilder deleteBottonContainer;
                TagBuilder deleteButton;
                TagBuilder uploadedImageIdValue;
                TagBuilder clear;

                int fileInputNum = 0;
                if (imageIds != null)
                {

                    fileInputNum = numberOfImages - imageIds.Length;


                    foreach (var id in imageIds)
                    {
                        imageContainer = new TagBuilder("div");
                        imageContainer.Attributes["class"] = "imageContainer";

                        //imageFrame = new TagBuilder("div");
                        //imageFrame.Attributes["class"] = "imageFrame";

                        image = new TagBuilder("img");
                      

                        //imageFrame.InnerHtml = image.ToString(TagRenderMode.SelfClosing);

                        deleteBottonContainer = new TagBuilder("div");
                        deleteBottonContainer.Attributes["class"] = "deleteButtonContainer";

                        deleteButton = new TagBuilder("a");
                        // removeLink.Attributes["onclick"] = "addNewFileInput(event)";
                        deleteButton.Attributes["href"] = "/" + deleteImageController + "/" + deleteImageAction + "/" + id;
                        deleteButton.InnerHtml = "ลบรูป";

                        deleteBottonContainer.InnerHtml = deleteButton.ToString();

                        uploadedImageIdValue = new TagBuilder("input");
                        uploadedImageIdValue.MergeAttributes(new RouteValueDictionary(new { type = "hidden", name = tagName, value = id.ToString() }));

                        clear = new TagBuilder("div");
                        clear.Attributes["style"] = "clear:both";

                        imageContainer.InnerHtml = image.ToString(TagRenderMode.SelfClosing) +
                        deleteBottonContainer.ToString() +
                        uploadedImageIdValue.ToString(TagRenderMode.SelfClosing) +
                        clear.ToString();
                        imageUploaderContainer.InnerHtml += imageContainer.ToString();
                    }

                }
                else
                {
                    fileInputNum = numberOfImages;
                }


                TagBuilder fileInputField;
                TagBuilder fileInput;

                for (int i = 0; i < fileInputNum; i++)
                {
                    fileInputField = new TagBuilder("div");
                    fileInputField.Attributes["class"] = "fileInputField";
                    fileInput = new TagBuilder("input");
                    fileInput.Attributes["name"] = tagName + i.ToString();
                    fileInput.MergeAttributes(new RouteValueDictionary(new { type = "file" }));
                    fileInputField.InnerHtml = fileInput.ToString(TagRenderMode.SelfClosing);
                    imageUploaderContainer.InnerHtml += fileInputField.ToString();

                }
                return imageUploaderContainer.ToString();
            }
            else
            {
                TagBuilder imageUploaderContainer = new TagBuilder("div");

                TagBuilder fileInputField;
                TagBuilder fileInput;


                for (int i = 0; i < numberOfImages; i++)
                {
                    fileInputField = new TagBuilder("div");
                    fileInputField.Attributes["class"] = "fileInputField";
                    fileInput = new TagBuilder("input");
                    fileInput.Attributes["name"] = tagName + i.ToString();
                    fileInput.MergeAttributes(new RouteValueDictionary(new { type = "file", @class = "fileInputField" }));
                    fileInputField.InnerHtml = fileInput.ToString(TagRenderMode.SelfClosing);
                    imageUploaderContainer.InnerHtml += fileInputField.ToString();

                }

                return imageUploaderContainer.ToString();

            }

        }
*/
        public static string FileUpload(this HtmlHelper helper, string name)
        {

            TagBuilder fileUpload = new TagBuilder("input");
            fileUpload.MergeAttributes(new RouteValueDictionary(new { type = "file", id = name, name = name }));

            return fileUpload.ToString(TagRenderMode.SelfClosing);
        }




        public static string Image(this HtmlHelper helper, string Action, string Controller, string imageId, string altText)
        {
            //TagBuilder builder = new TagBuilder("image");
            //builder.Attributes.Add("src", "/" + Controller + "/" + Action +"/" + imageId);
            //builder.Attributes.Add("alt", altText);         
            // return builder.ToString(TagRenderMode.SelfClosing);
            return Image(helper, Action, Controller, imageId, altText, null);
        }


        public static string Image(this HtmlHelper helper, string Action, string Controller, string imageId, string altText, object htmlAttributes)
        {
            TagBuilder builder = new TagBuilder("img");
            builder.Attributes.Add("src", "/" + Controller + "/" + Action + "/" + imageId);
            builder.Attributes.Add("alt", altText);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            return builder.ToString(TagRenderMode.SelfClosing);
        }

        public static string ActionButton(this HtmlHelper helper, string value, string Action, string Controller)
        {

            TagBuilder button = new TagBuilder("input");
            button.MergeAttributes(new RouteValueDictionary(new { type = "button", value = value, onclick = "window.location='/" + Controller + "/" + Action + "'" }));

            return button.ToString(TagRenderMode.SelfClosing);
        }


        public static string SubmitButton(this HtmlHelper helper, string value)
        {

            TagBuilder button = new TagBuilder("input");
            button.MergeAttributes(new RouteValueDictionary(new { type = "submit", value = value }));

            return button.ToString(TagRenderMode.SelfClosing);
        }



        public static string ActionCheckBox(this HtmlHelper helper,
            string name, object value, string actionURL)
        {
            TagBuilder checkbox = new TagBuilder("input");
            checkbox.MergeAttributes(new RouteValueDictionary(new
            {
                value = value,
                type = "checkbox",
                onclick = actionURL
            }));

            if (helper.ViewData.Model != null)
            {
                Type obj = helper.ViewData.Model.GetType();
                PropertyInfo property = obj.GetProperty(name);
                string propertyValue = property.GetValue(obj, null).ToString();
                if (propertyValue == value.ToString())
                {
                    checkbox.MergeAttribute("checked", "checked");
                }


            }




            return checkbox.ToString(TagRenderMode.SelfClosing);
        }


        public static string Avatar(this HtmlHelper helper, Avatar avatar, string logonUserName)
        {
            //todo improve avatar
            logonUserName = logonUserName ?? string.Empty;


            Func<string> showAddFriend = () =>
            {

                if (avatar.UserName == logonUserName)
                {
                    return "<br /><a href='#' name= class='addFriend'>เพิ่มเพือน</a>";

                }
                else
                {
                    return "<br /><a href='#' name="+ avatar.UserName + " class='addFriend'>เพิ่มเพือน</a>";
                    //return string.Empty;
                }
            };

            string tag = "<div class='forumUserProfile'>" +
            "<span class='username'>" + avatar.UserName + "</span>" +
            "<div class='avatar'>" +
            "<img src='" + avatar.AvatarImageURL + "'/>" +
            showAddFriend()
            + "</div>" +
            "<div class='userProfile'>" +
                "<table><tr><th>ขั้น</th><td>" + avatar.Level + "</td></tr>" +
                   "<th>ตำแหน่ง</th><td>" + avatar.Position + "</td></tr>" +
                "<th>ตั้งกระทู้</th><td>" + avatar.Posts + "</td></tr>" +
                "<th>ตอบกระทู้</th><td>" + avatar.Replies + "</td></tr>" +
                "<th>กาก</th><td>" + avatar.Money + "</td></tr>" +
                "<th>แต้ม</th><td>" + avatar.Points + "</td></tr></table>" +
            "</div>" + "</div>";





            return tag;
        }




    }


    public class Avatar
    {
        private int points;
        public string UserName { get; set; }
        public int Money { get; set; }
        public int Points
        {
            get
            {
                return points;
            }

            set
            {
                points = value;
                if (points > 3000001)
                {

                    Level = "เทพสราดดด";
                    Position = " ประธานสโมสร";
                }
                else if (points >= 2000001)
                {
                    Level = "มูเรียกพ่อ";
                    Position = "ผู้จัดการทีม";
                }
                else if (points >= 1000001)
                {
                    Level = "แมร่งโปรว่ะ";
                    Position = "สต๊าฟโค้ช";
                }
                else if (points >= 500001)
                {
                    Level = "แหล่มเลย";
                    Position = "ทีมชุดใหญ่";
                }
                else if (points >= 200001)
                {
                    Level = "เทพลูคัส";
                    Position = "ทีมสำรอง";
                }
                else if (points >= 100001)
                {
                    Level = "มีแววนะเนี่ย";
                    Position = "ดาวรุ่งพุ่งแรง";
                }
                else if (points >= 30001)
                {
                    Level = "เริ่มเข้าขั้น";
                    Position = "ทีมเยาวชน";
                }

                else if (points >= 10001)
                {
                    Level = "พอไปวัดไปวา";
                    Position = "เด็กฝึกหัด";
                }

                else if (points >= 4001)
                {
                    Level = "นักเตะข้างถนน";
                    Position = "สมัครเล่น";
                }
                else
                {
                    Level = "อ่อน";
                    Position = "เด็กบอลวัด";
                }
            }


        }
        public string AvatarImageURL { get; set; }

        public string Level { get; set; }

        public string Position { get; set; }


        public int Posts { get; set; }
        public int Replies { get; set; }



        public Avatar(string userName, string avatarImageURL, int money, int points, int posts, int replies)
        {
            UserName = userName;
            AvatarImageURL = avatarImageURL;
            Money = money;
            Points = points;
            Posts = posts;
            Replies = replies;
        }

    }

    public class PaginationModel
    {
        public int PageNo { get; set; }
        public int ItemPerPage { get; set; }
        public string ActionURL { get; set; }
        public int ModelSize { get; set; }

        public PaginationModel(int pageNo, int itemPerPage, int modelSize, string actionURL)
        {
            PageNo = pageNo;
            ItemPerPage = itemPerPage;
            ActionURL = actionURL;
            ModelSize = modelSize;

        }

    }
}

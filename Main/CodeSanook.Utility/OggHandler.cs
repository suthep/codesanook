﻿using System;
using System.Web;
using System.Web.Routing;

namespace CodeSanook.Utility
{
    public class OggHandler : IRouteHandler, IHttpHandler
    {

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }


        /// <summary>
        /// process ogg file
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            var request = context.Request;
            var response = context.Response;


            try
            {
                //response ogg file with Content Type Header
                response.ContentType = "application/ogg";
                response.WriteFile(request.PhysicalPath);
            }
            catch (Exception ex)
            {
                response.Write("<html>\r\n");
                response.Write("<head><title>Ogg HTTP Handler</title></head>\r\n");
                response.Write("<body>\r\n");
                response.Write("<h1>" + ex.Message + "</h1>\r\n");
                response.Write("</body>\r\n");
                response.Write("</html>");
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }




    }
}
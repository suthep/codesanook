﻿using System.Reflection;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using CodeSanook.MicroBlog.Tests.Unit.Builders.RepositoryBuilders;
using Moq;
using NUnit.Framework;
using log4net;
using Autofac;
using log4net.Config;

namespace CodeSanook.MicroBlog.Tests.Unit
{
    [TestFixture]
    public abstract class UnitTestFixtureBase
    {
        protected ILog _log = LogManager.GetLogger("UnitTestFixtureBase");
        protected IContainer Container;
        protected ILifetimeScope Scope;
        protected UserRepositoryBuilder UserRepositoryBuilder;
        protected ArticleRepositoryBuilder ArticleRepositoryBuilder;

        [TestFixtureSetUp]
        public virtual void TestFixtureSetUp()
        {
            XmlConfigurator.Configure();

            SetUpAutofac();
            Scope = Container.BeginLifetimeScope();
        }


        [TestFixtureTearDown]
        public virtual void TestFixtureTearDown()
        {
            Scope.Dispose();
        }


        [SetUp]
        public virtual void SetUp()
        {

        }

        [TearDown]
        public virtual void TearDown()
        {
            UserRepositoryBuilder.Clear();
            ArticleRepositoryBuilder.Clear();
        }

        private void SetUpAutofac()
        {

            var builder = new ContainerBuilder();

            //var repository = Assembly.GetAssembly(typeof(IUserRepository));
            //builder.RegisterAssemblyTypes(repository)
            //    .Where(t => t.Name.EndsWith("Repository"))
            //    .AsImplementedInterfaces();

            UserRepositoryBuilder = new UserRepositoryBuilder();
            var userRepository = UserRepositoryBuilder.Create().Build();
            builder.RegisterInstance(userRepository).As<IUserRepository>();


            ArticleRepositoryBuilder = new ArticleRepositoryBuilder();
            var articleRepository = ArticleRepositoryBuilder.Create().Build();
            builder.RegisterInstance(articleRepository).As<IArticleRepository>();

            var businessLayer = Assembly.GetAssembly(typeof(UserService));
            builder.RegisterAssemblyTypes(businessLayer)
                .Where(t => t.Name.EndsWith("Service"))
                .PropertiesAutowired();

            var unitOfWorkFactoryMock = new Mock<IUnitOfWorkFactory>();
            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWorkFactoryMock.Setup(u => u.Create()).Returns(() =>  unitOfWork.Object);

            builder.RegisterInstance(unitOfWorkFactoryMock.Object).As<IUnitOfWorkFactory>();
            var log = LogManager.GetLogger(typeof(UnitTestFixtureBase).Namespace);
            builder.RegisterInstance(log).As<ILog>();

            Container = builder.Build();

        }
    }
}
﻿using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;

namespace CodeSanook.MicroBlog.Tests.Unit.Builders
{
    public class UserBuilder : BuilderBase<UserBuilder, User>
    {
        private string _email;
        private string _password;

        public override UserBuilder Create()
        {
            return this;
        }

        public  UserBuilder Create(string email,string password)
        {
            _email = email;
            _password = password;
            return this;
        }
        public UserBuilder WithPassword(string password)
        {
            _password = password;
            return this;
        }

        public override User Build()
        {
            var user = new User()
            {
                Email = _email,
                Password = _password,
                
            };
            return user;
        }

        public override void Clear()
        {
        }
    }
}
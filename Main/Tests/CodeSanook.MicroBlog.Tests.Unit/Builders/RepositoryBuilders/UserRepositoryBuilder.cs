﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using Moq;

namespace CodeSanook.MicroBlog.Tests.Unit.Builders.RepositoryBuilders
{
    public class UserRepositoryBuilder:BuilderBase<UserRepositoryBuilder,IUserRepository>
    {
        public Mock<IUserRepository> UserRepositoryMock { get; set; }
        public IList<User> UserList { get; set; }
        public IList<Role> RoleList { get; set; }
        public IList<UserProfile> UserProfileList { get; set; }


        public UserRepositoryBuilder()
        {
            UserRepositoryMock = new Mock<IUserRepository>();
            UserList = new List<User>();
            RoleList = new List<Role>();
        }

        public override UserRepositoryBuilder Create()
        {
            SetupAdd();
            SetupFindAll();
            SetupFindById();

            SetupAddRole();
            SetupGetRoleByName();

            return this;
        }


        public override IUserRepository Build()
        {
            return UserRepositoryMock.Object;
        }

        public override void Clear()
        {
            UserList.Clear();
            RoleList.Clear();
        }

        private void SetupAdd()
        {
            UserRepositoryMock.Setup(u => u.Add(
                It.IsAny<User>()))
                .Callback((User user) =>
                              {
                                  user.Id = Guid.NewGuid();
                                  UserList.Add(user);
                                  if (user.UserProfile != null)
                                  {
                                      var profile = user.UserProfile;
                                      profile.Id = user.Id;
                                      UserProfileList.Add(profile);
                                  }
                              });


        }

        private void SetupFindAll()
        {
            UserRepositoryMock.Setup(u => u.FindAll())
                .Returns(() => {
                    return UserList;
                });

        }

        private void SetupFindById()
        {

            UserRepositoryMock.Setup(u => u.FindById(
                It.IsAny<Guid>()))
                .Returns((Guid userId) => {
                    var user = UserList.SingleOrDefault(u => u.Id == userId);
                    return user;
                });

        }

        private void SetupAddRole()
        {
            UserRepositoryMock.Setup(u => u.AddRole(
                It.IsAny<Role>()))
                .Callback(( Role role ) => {
                    RoleList.Add(role);
                });


        }

        private void SetupGetRoleByName()
        {
            UserRepositoryMock.Setup(u => u.GetRoleByName(
                It.IsAny<string>()))
                .Returns((string roleName) =>
                             {
                                 var role = RoleList.SingleOrDefault(r => r.Name == roleName);
                                 return role;
                             });

        }
        
    }
}
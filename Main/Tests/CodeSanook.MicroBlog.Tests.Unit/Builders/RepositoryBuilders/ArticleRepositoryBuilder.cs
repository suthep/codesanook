﻿using System.Collections.Generic;
using System.Linq;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using Moq;

namespace CodeSanook.MicroBlog.Tests.Unit.Builders.RepositoryBuilders
{
    public class ArticleRepositoryBuilder : BuilderBase<ArticleRepositoryBuilder, IArticleRepository>
    {

        public IList<Article> ArticleList { get; set; }
        public IList<Tag> TagList { get; set; }

        public Mock<IArticleRepository> ArticleRepositoryMock { get; set; }

        public ArticleRepositoryBuilder()
        {
            ArticleList = new List<Article>();
            TagList = new List<Tag>();

            ArticleRepositoryMock = new Mock<IArticleRepository>();
        }

        public override ArticleRepositoryBuilder Create()
        {
            SetupAdd();
            SetupFindAll();
            SetupGetNumberOfArticles();
            SetupGetByAlias();
            SetupDelete();
            SetupFindById();
            SetupAddTag();

            return this;
        }

        public override IArticleRepository Build()
        {
            return ArticleRepositoryMock.Object;
        }

        public override void Clear()
        {
            ArticleList.Clear();
        }

        private void SetupAdd()
        {
            ArticleRepositoryMock.Setup(a => a.Add(
                It.IsAny<Article>()))
                .Callback((Article article) =>
                {
                    article.Id = ArticleList.Count + 1;
                    ArticleList.Add(article);
                });

        }

        private void SetupFindAll()
        {
            ArticleRepositoryMock.Setup(a => a.FindAll()).Returns(() => ArticleList);

            ArticleRepositoryMock.Setup(a => a.FindAll(
               It.IsAny<int>(),
               It.IsAny<int>()))
               .Returns((int index, int count) =>
                               {
                                   var result = ArticleList;//.Skip(index * count).Take(count);
                                   return result;
                               });
        }

        private void SetupGetNumberOfArticles()
        {
            ArticleRepositoryMock.Setup(a => a.GetNumberOfArticles())
                .Returns(() => ArticleList.Count);

        }

        private void SetupGetByAlias()
        {
            ArticleRepositoryMock.Setup(a => a.GetByAlias(
                It.IsAny<string>()))
                .Returns((string alias) => ArticleList.SingleOrDefault(ar => ar.Alias == alias));

        }

        private void SetupDelete()
        {
            ArticleRepositoryMock.Setup(a => a.Remove(
                It.IsAny<Article>()))
                .Callback((Article article) =>

                              {
                                  var result = ArticleList.Remove(article);
                                  Log.DebugFormat("remove result: {0}",result);
                              });
        }

        private void SetupFindById()
        {

            ArticleRepositoryMock.Setup(a => a.FindById(
                It.IsAny<int>()))
                .Returns((int articleId) => ArticleList.SingleOrDefault(ar =>ar.Id == articleId));

        }

        public void SetupAddTag()
        {
            ArticleRepositoryMock.Setup(a => a.AddTag(
                It.IsAny<Tag>()))
                .Callback((Tag tag) => TagList.Add(tag));
        }
    }
}
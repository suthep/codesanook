﻿using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Unit.Services
{
    public class ArticleServiceTest : UnitTestFixtureBase
    {
        private ArticleService _articleService;
        private IArticleRepository _articleRepository;

        public override void TestFixtureSetUp()
        {
            base.TestFixtureSetUp();
            _articleService = Scope.Resolve<ArticleService>();
            _articleRepository = Scope.Resolve<IArticleRepository>();
        }


        [Test]
        public void CreateArticle_ValidInput_ArticleCountEqual1()
        {
            var article = new ArticleBuilder().Create().Build();
            _articleService.CreateArticle(article);
            _articleRepository.FindAll().Count().Should().Be(1);
        }

        [Test]
        public void GetArticleList_1ArticleExist_1ArticleReturn()
        {
            var article = new ArticleBuilder().Create().Build();
            _articleService.CreateArticle(article);

            var pageNo =1;
            var itemPerPage = 5;
           var response =  _articleService.GetArticleList(pageNo,itemPerPage);
           response.IsSuccess.Should().BeTrue();
           response.Content.ArticleList.Count().Should().Be(1);
           response.Content.NumberOfArticles.Should().Be(1);
        }



        [Test]
        public void GetArticleDetails_1ArticleExist_1ArticleReturn()
        {
            var article = new ArticleBuilder()
                .Create()
                .WithAlias("love")
                .Build();
            _articleService.CreateArticle(article);

            var response = _articleService.GetArticleDetail(article.Alias);
           response.IsSuccess.Should().BeTrue();
           response.Content.Id.Should().Be(1);
           response.Content.Alias.Should().Be(article.Alias);
        }

        [Test]
        public void DeleteArticle_1ArticleExist_NoArticleLeft()
        {
            var article = new ArticleBuilder()
                .Create()
                .WithAlias("love")
                .Build();
            _articleService.CreateArticle(article);

            var response = _articleService.DeleteArticle(article.Id);
           response.IsSuccess.Should().BeTrue();
           _articleRepository.FindAll().Count().Should().Be(0);
        }



    }
}
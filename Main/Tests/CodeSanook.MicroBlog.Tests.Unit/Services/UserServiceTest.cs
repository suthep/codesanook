﻿using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Unit.Services
{
    public class UserServiceTest : UnitTestFixtureBase
    {
        private IUserRepository _userRepository;
        private UserService _userService;

        public override void TestFixtureSetUp()
        {
            base.TestFixtureSetUp();
            _userRepository = Scope.Resolve<IUserRepository>();
            _userService = Scope.Resolve<UserService>();
        }

        [Test]
        public void Add_ValidInput_UserAdded()
        {
            var user = new UserBuilder()
                .Create()
                .Build();

            _userRepository.Add(user);
            _userRepository.FindAll().Count().Should().Be(1);

        }

        [Test]
        public void RegisterTest_ValidInput_UserRegistered()
        {
            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userService.Register(user);
            _userRepository.FindAll().Count().Should().Be(1);

        }

        [Test]
        public void SetUserActiveStatus_SetToTrue_UserIsActiveIsTrue()
        {
            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userRepository.Add(user);

            _userService.SetUserActiveStatus(user.Id, true);

            user = _userRepository.FindById(user.Id);
            user.IsActive.Should().BeTrue();

        }

        [Test]
        public void SetUserActiveStatus_SetToFalse_UserIsActiveIsFalse()
        {
            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userRepository.Add(user);

            _userService.SetUserActiveStatus(user.Id, false);

            user = _userRepository.FindById(user.Id);
            user.IsActive.Should().BeFalse();

        }


        [Test]
        public void SetUserActivated_ExistingUser_UserIsActivatedIsTrue()
        {
            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userRepository.Add(user);

            _userService.SetUserAlreadyActivated(user.Id);

            user = _userRepository.FindById(user.Id);
            user.IsActivated.Should().BeTrue();

        }

        [Test]
        public void GetAddRole_ValidRoleName_NewRoleSaved()
        {
            var roleName = "admin";
            _userService.AddRole(roleName);

            var role = _userRepository.GetRoleByName("admin");
            role.Name.Should().Be(roleName);
        }

        [Test]
        public void AddUserToRoles_RoleExist_UserHasRole()
        {
            var roleName = "admin";
            var role = new Role()
            {
                Name = roleName,
            };
            _userRepository.AddRole(role);

            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userRepository.Add(user);
            _userService.AddUserToRoles(user.Id, new[] { roleName });

            user.Roles.Count.Should().Be(1);
            user.Roles.Should().Contain(role);
        }


        [Test]
        public void IsUserInRole_ValidRole_UserInRole()
        {
            var roleName = "admin";
            var role = new Role()
            {
                Name = roleName,
            };
            _userRepository.AddRole(role);

            var user = new UserBuilder()
            .Create()
            .WithPassword("1234")
            .Build();
            _userRepository.Add(user);
            _userService.AddUserToRoles(user.Id, new[] { roleName });

            var response = _userService.IsUserInRole(user.Id, new[] { roleName });
            response.Content.Should().BeTrue();
        }


        [Test]
        public void RemoveUserFromRole_UserNotInRole()
        {
            var roleName = "admin";
            var role = new Role()
                           {
                               Name = roleName,
                           };
            _userRepository.AddRole(role);

            var user = new UserBuilder()
                .Create()
                .WithPassword("1234")
                .Build();
            _userRepository.Add(user);
            _userService.AddUserToRoles(user.Id, new[] { roleName });

            _userService.RemoveUserFromRoles(user.Id, new[] { roleName });


            var response = _userService.IsUserInRole(user.Id, new[] { roleName });
            response.Content.Should().BeFalse();
        }

    }
}
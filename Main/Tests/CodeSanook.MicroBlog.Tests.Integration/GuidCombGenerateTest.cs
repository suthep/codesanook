﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Integration
{
    public class GuidCombGenerateTest : IntegrationTestFixtureBase
    {
        private IUserRepository _userRepository;

        public override void TestFixtureSetup()
        {
            base.TestFixtureSetup();
            _userRepository = Scope.Resolve<IUserRepository>();
        }

        /// <summary>
        /// useful links
        /// http://nhforge.org/blogs/nhibernate/archive/2009/05/21/using-the-guid-comb-identifier-strategy.aspx
        /// http://www.informit.com/articles/article.aspx?p=25862
        /// http://stackoverflow.com/questions/1995579/how-do-i-use-the-guid-comb-strategy-in-a-mysql-db
        /// http://ayende.com/blog/3915/nhibernate-avoid-identity-generator-when-possible
        /// 
        /// </summary>
        [Test]
        public void GenerateGuidCombTest()
        {
            User author =null;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                for (var i = 0; i < 10; i++)
                {
                    author = new UserBuilder()
                        .Create("theeranitp@gmail.com", "1234")
                        .Build();
                    _userRepository.Add(author);
                    Log.DebugFormat("generated author Guid.Comb: {0}", author.Id);
                }

                unitOfWork.Commit();

            }

            
        }

    }

}
﻿using System;
using System.Data.SqlClient;
using System.Reflection;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Repositories.NH;
using CodeSanook.MicroBlog.Repositories.NH.Repositories;
using Moq;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using log4net;
using log4net.Config;

namespace CodeSanook.MicroBlog.Tests.Integration
{
    [TestFixture]
    public class IntegrationTestFixtureBase
    {
        protected ILog Log = LogManager.GetLogger("IntegrationTestFixtureBase");
        protected IContainer Config;
        protected ILifetimeScope Scope;
        protected NHUnitOfWorkFactory UnitOfWorkFactory;
        private const string connectionStringToMasterDatabase =
@"Server=.\SQLEXPRESS;Integrated security=SSPI;database=master";

        private void SetupAutofac()
        {
            var builder = new ContainerBuilder();
            var dataAccess = Assembly.GetAssembly(typeof(ArticleRepository));
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces();

            var service = Assembly.GetAssembly(typeof(ArticleService));
            builder.RegisterAssemblyTypes(service)
                .Where(t => t.Name.EndsWith("Service"))
                .PropertiesAutowired();

            //unit of work
            UnitOfWorkFactory = new NHUnitOfWorkFactory();
            builder.RegisterInstance(UnitOfWorkFactory).As<IUnitOfWorkFactory>();

            var log = LogManager.GetLogger(typeof(IntegrationTestFixtureBase).Namespace);
            builder.RegisterInstance(log).As<ILog>();

            Config = builder.Build();
        }


        [TestFixtureSetUp]
        public virtual void TestFixtureSetup()
        {
            //CreateDatabase();
            XmlConfigurator.Configure();
            SessionFactory.Initialize();
            SetupAutofac();

            //drop all table
            using(UnitOfWorkFactory.Create())
            {
                var session = SessionFactory.GetCurrentSession();
                session.ExecuteSqlFile("SqlScripts/DropAllForeignKeyConstraintsSqlDatabase.sql");
                session.ExecuteSqlFile("SqlScripts/DropAllTablesSqlDatabase.sql");
            }

            //create table
            var schemaExport = new SchemaExport(SessionFactory.Config);
            schemaExport.SetOutputFile("db_create.sql");
            schemaExport.Create(true, true);

            //create additional indexs
            using(UnitOfWorkFactory.Create())
            {
                var session = SessionFactory.GetCurrentSession();
                session.ExecuteSqlFile("SqlScripts/CreateClusterIndexesSqlDatabase.sql");
            }

            Scope = Config.BeginLifetimeScope();
        }

        [TestFixtureTearDown]
        public virtual void TestFxitureTearDown()
        {
            Scope.Dispose();
        }

        [SetUp]
        public virtual void SetUp()
        {


            using (UnitOfWorkFactory.Create())
            {
                var session = SessionFactory.GetCurrentSession();
                session.ExecuteSqlFile("SqlScripts/TruncateAllTablesSqlDatabase.sql");
            }
        }

        [TearDown]
        public virtual void TearDown()
        {
        }


        private void CreateDatabase()
        {
            try
            {
                //check if exist
                using (var con = new SqlConnection(connectionStringToMasterDatabase))
                using (var command = con.CreateCommand())
                {
                    var sql =
@"if db_id('CodeSanookTestDb') is not null
BEGIN
SELECT 1
END
ELSE 
BEGIN
SELECT 0
END";
                    command.CommandText = sql;
                    con.Open();

                    var result = (int)command.ExecuteScalar();

                    if (result == 1)
                    {
                        Log.Debug("database exist");
                        return; //database exist
                    }
                }

                using (var con = new SqlConnection(connectionStringToMasterDatabase))
                using (var command = con.CreateCommand())
                {
                    //var sql = @"CREATE DATABASE CodeSanookTestDb ON PRIMARY " +
                    //      "(NAME = CodeSanookTestDb, " +
                    //      "FILENAME = 'C:\\CodeSanookTestDb.mdf', " +
                    //      "SIZE = 3MB, MAXSIZE = 10MB, FILEGROWTH = 10%) " +
                    //      "LOG ON (NAME = CodeSanookTestDb_Log, " +
                    //      "FILENAME = 'C:\\CodeSanookTestDb.ldf', " +
                    //      "SIZE = 1MB, " +
                    //      "MAXSIZE = 5MB, " +
                    //      "FILEGROWTH = 10%)";
                    var sql = " CREATE DATABASE CodeSanookTestDb";
                    command.CommandText = sql;
                    con.Open();

                    command.ExecuteNonQuery();
                    Log.Debug("DataBase is Created Successfully");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private void DropDatabase()
        {
            try
            {


                //check if exist
                using (var con = new SqlConnection(connectionStringToMasterDatabase))
                using (var command = con.CreateCommand())
                {
                    var sql =
@"if db_id('CodeSanookTestDb') is not null
BEGIN
SELECT 1
END
ELSE 
BEGIN
SELECT 0
END";
                    command.CommandText = sql;
                    con.Open();

                    var result = (int)command.ExecuteScalar();

                    if (result == 0)
                    {
                        Log.Debug("no database to drop");
                        return; //database exist
                    }
                }

                //check if exist
                using (var con = new SqlConnection(connectionStringToMasterDatabase))
                using (var command = con.CreateCommand())
                {
                    var sql = "DROP DATABASE CodeSanookTestDb";
                    command.CommandText = sql;
                    con.Open();

                    command.ExecuteNonQuery();
                    Log.Debug("database drop successfully");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }


    }
}

﻿using System;

namespace CodeSanook.MicroBlog.Infrastructure
{
    public interface IUtcCreateDate
    {
        DateTime UtcCreateDate { get; set; }
    }
}
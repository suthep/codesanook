﻿using log4net;

namespace CodeSanook.MicroBlog.Infrastructure
{
    public abstract class BuilderBase<TB,TR>
    {
        protected ILog Log = LogManager.GetLogger("Builder");

        public abstract TB Create();
        public abstract TR Build();
        public abstract void Clear();
    }
}
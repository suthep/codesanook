﻿using System;
using System.Collections;
using System.Net.Mail;
using System.Net;

namespace CodeSanook.MicroBlog.Business.Services
{
    public class MailService
    {

        private readonly MailAddress from = 
            new MailAddress("admin@codesanook.com", "codesanook.com administrator");
        private readonly SmtpClient client = new SmtpClient("mail.codesanook.com");
        private readonly NetworkCredential credentials = new NetworkCredential("admin@codesanook.com", "44jkws44jkws");

        private MailMessage message;

        /// <summary>
        /// instanciate MailService object
        /// </summary>
        /// <param name="to">email address to send message</param>
        public MailService(string  toEmail)
        {
            message = new MailMessage(from, new MailAddress(toEmail));

        }

        /// <summary>
        /// send mail to multiple emails
        /// </summary>
        /// <param name="toEmails"></param>
        public MailService(string[] toEmails)
        {
            message = new MailMessage();
            message.From = from;

            foreach (string email in toEmails)
            {
                message.To.Add(email);
            }

        }
        /// <summary>
        /// set subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// set body of mail supported html
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// send message
        /// </summary>
        public void Send()
        {
            message.Subject = Subject;
            message.Body = Body;
            message.IsBodyHtml = true; //send in html format

            client.Credentials = credentials;

            try
            {
                client.Send(message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }



        }



        public void SendEmail()
        {
            throw new NotImplementedException();
                    //send mail
                    //var templateVars = new Hashtable();
                    //templateVars.Add("userName", model.UserName);
                    //templateVars.Add("email", model.Email);
                    //templateVars.Add("details", model.Details);
                    //templateVars.Add("articleTitle", model.Article.Title);
                    //templateVars.Add("articleUrl", string.Format("http://codesanook.com/article/details/{0}",
                    //    model.Article.Alias));

                    //var parser = new Parser(Server.MapPath("~/MailTemplates/NewComment.htm"), templateVars);

                    //var mailService = new MailService("admin@codesanook.com");
                    //mailService.Subject = "new comment for " + model.Article.Title;
                    //mailService.Body = parser.Parse();

                    //try
                    //{
                    //    mailService.Send();
                    //}
                    //catch (Exception e)
                    //{
                    //    ModelState.AddModelError("", e.Message);
                    //}
        }
    }
}

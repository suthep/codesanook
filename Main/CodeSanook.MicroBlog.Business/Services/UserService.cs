﻿using System;
using System.Linq;
using System.Web;
using CodeSanook.MicroBlog.Business.Messages.Responses;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.Utility;
using log4net;

namespace CodeSanook.MicroBlog.Business.Services
{
    /// <summary>
    /// class ที่ใช้จัดการกับสมาชิก
    /// </summary>
    public class UserService
    {
        private readonly ILog _log = LogManager.GetLogger("Service");

        private const string USER_COOKIE_NAME = "userService";
        public IUnitOfWorkFactory UnitOfWorkFactory { get; set; }
        public IUserRepository UserRepository { get; set; }


        /// <summary>
        /// ลงทะเบียนสมาชิกใหม่
        /// </summary>
        /// <param name="user">member object</param>
        public bool Register(User user)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {

                    user.Password = HashHelper.GetHash(user.Password, HashHelper.HashType.SHA256);
                    UserRepository.Add(user);

                    unitOfWork.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return false;
                }
            }
        }


        /// <summary>
        /// activate สมาชิกหลังจากกด link ที่ได้รับใน email
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        public void SetUserAlreadyActivated(Guid userId)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);
                    user.IsActivated = true;
                    unitOfWork.Commit();

                }
                catch (Exception e)
                {
                    _log.Error(e.Message);

                }
            }
        }

        /// <summary>
        /// ตั้งให้สมาชิกอยู่ในสถานะ active สามารถ login เข้าสู่ระบบได้ตามปกติ
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        public void SetUserActiveStatus(Guid userId, bool isActive)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);
                    user.IsActive = isActive;
                    unitOfWork.Commit();
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }




        /// <summary>
        /// สมาชิก log on เข้าสู่ระบบ
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        /// <param name="remember">จดจำสมาชิก</param>
        public void LogIn( string email, bool remember)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var cookieId = Guid.NewGuid().ToString();
                    //save new cookieId to db
                    var user = UserRepository.FindByEmail(email);
                    user.CookieId = cookieId;
                    unitOfWork.Commit();

                    //create new cookie
                    var encryptedText = RijndaelEncrypt.Encrypt(cookieId, true);
                    var userLogInCookie = new HttpCookie(USER_COOKIE_NAME);
                    userLogInCookie.Value = encryptedText;

                    if (remember) //expire next month if no cookie with session
                    {
                        userLogInCookie.Expires = DateTime.Now.AddMonths(1);
                    }

                    //add to response
                    HttpContext.Current.Response.Cookies.Add(userLogInCookie);
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }

        /// <summary>
        /// สมาชิก log out ออกจากระบบ
        /// </summary>
        public void LogOut()
        {
            try
            {
                var userCookie = HttpContext.Current.Request.Cookies[USER_COOKIE_NAME];
                if (userCookie != null)
                {
                    //set to date in the past
                    userCookie.Expires = DateTime.Now.AddMonths(-1);
                    HttpContext.Current.Response.Cookies.Add(userCookie);
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message);
            }
        }


        /// <summary>
        /// ตรวจสอบว่าสมาชิกได้ log on อยู่แล้วหรือไม่
        /// </summary>
        /// <returns>true ถ้าสมาชิกได้ log on อยู่แล้ว</returns>
        public bool IsLoggedIn()
        {
            using (UnitOfWorkFactory.Create())
            {
                try
                {
                    var userCookie = HttpContext.Current.Request.Cookies[USER_COOKIE_NAME];
                    if (userCookie != null && userCookie.Value != null)
                    {
                        //check is valid user
                        var cookieId = RijndaelEncrypt.Decrypt(userCookie.Value, true);
                        var user = UserRepository.GetByCookieId(cookieId);
                        return user != null;
                    }

                    return false;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// นำข้อมูลสมาชิกที่ log on อยู่ออกมา
        /// </summary>
        /// <returns>Member object</returns>
        public User GetLoggedInUser()
        {
            using (UnitOfWorkFactory.Create())
            {
                try
                {
                    var userCoookie = HttpContext.Current.Request.Cookies[USER_COOKIE_NAME];
                    if (userCoookie != null && userCoookie.Value != null)
                    {
                        //check is valid user
                        var cookieId = RijndaelEncrypt.Decrypt(userCoookie.Value, true);

                        var user = UserRepository.GetByCookieId(cookieId);
                        return user;
                    }
                    return null;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// ตรวจสอบ email และ pasword ที่ใช้ในการ logon ของสมาชิก
        /// </summary>
        /// <param name="email">email เพื่อใช้ในการ log on ของสมาชิก</param>
        /// <param name="password">password เพื่อใช้ในการ log on ของสมาชิก</param>
        /// <returns>true หาก email และ password ถูกต้อง</returns>
        public bool ValidateUserLogIn(string email, string password)
        {
            using (UnitOfWorkFactory.Create())
            {
                try
                {
                    //todo hash value to encrypt 
                    password = HashHelper.GetHash(password, HashHelper.HashType.SHA256);
                    var rowCount = UserRepository.GetRowCountUserWithEmailAndPassword(email, password);
                    if (rowCount == 1)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// reset password ใหม่ให้กับสมาชิก กรณีที่ลืม password
        /// </summary>
        /// <param name="memberId">id ของสมาชิก</param>
        /// <returns>password ใหม่</returns>
        public string ResetPassword(Guid userId)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);

                    if (user != null)
                    {
                        var randomPassword = HashHelper.GeneratePassword(6);
                        //save new password 
                        var hashPassword = HashHelper.GetHash(randomPassword, HashHelper.HashType.SHA256);
                        user.Password = hashPassword;
                        unitOfWork.Commit();

                        return randomPassword;
                    }
                    else
                    {
                        throw new InvalidOperationException(string.Format("no user with id: {0}", userId));
                    }

                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// เปลี่ยน password
        /// </summary>
        /// <param name="email">email ที่ใช้ log on</param>
        /// <param name="oldPassword">password เดิมที่ใช้ log on</param>
        /// <param name="newPassword">password ใหม่ที่ต้องการใช้</param>
        /// <returns>true เมื่อการเปลี่ยน password ใหม่ทำได้สำเร็จ</returns>
        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    if (!ValidateUserLogIn(email, oldPassword))
                    {
                        return false;
                    }

                    //set new password
                    //get hash password
                    var hashPassword = HashHelper.GetHash(newPassword, HashHelper.HashType.SHA256);

                    var user = UserRepository.GetByEmail(email);
                    //set new password
                    user.Password = hashPassword;

                    //save
                    unitOfWork.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    return false;
                }
            }
        }


        public void AddRole(string roleName)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var newRole = new Role() { Name = roleName };
                    UserRepository.AddRole(newRole);

                    unitOfWork.Commit();
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }


        /// <summary>
        /// เพิ่ม role ให้กับสมาชิก
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        /// <param name="roleNames">ชื่อ role</param>
        public void AddUserToRoles(Guid userId, string[] roleNames)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);
                    foreach (var roleName in roleNames)
                    {
                        var role = UserRepository.GetRoleByName(roleName);
                        role.Users.Add(user);
                        user.Roles.Add(role);
                    }

                    unitOfWork.Commit();
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }

        /// <summary>
        /// ตรวจสอบว่าสมาชิกอยู่ใน role หรือไม่
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        /// <param name="roleNames">role ของ user user มีได้หลาย role</param>
        /// <returns>true เมื่อสมาชิกอยู่ใน role ที่กำหนดให้</returns>
        public ServiceResponse<bool> IsUserInRole(Guid userId, string[] roleNames)
        {
            using (UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);

                    if (user != null)
                    {
                        var result = user.Roles.Any(r => roleNames.Contains(r.Name.ToLower()));
                        return new ServiceResponse<bool>(result);
                    }

                    return new ServiceResponse<bool>("no user with");

                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message);
                    return new ServiceResponse<bool>(ex);
                }
            }
        }

        /// <summary>
        /// นำสมาชิกออกจาก role
        /// </summary>
        /// <param name="userId">id ของสมาชิก</param>
        /// <param name="roleNames">ชื่อ role</param>
        public void RemoveUserFromRoles(Guid userId, string[] roleNames)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var user = UserRepository.FindById(userId);
                    foreach (var roleName in roleNames)
                    {
                        var role = UserRepository.GetRoleByName(roleName);
                        role.Users.Remove(user); //add member to each role
                        user.Roles.Remove(role);
                    }

                    unitOfWork.Commit();
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }


    }//end class
}

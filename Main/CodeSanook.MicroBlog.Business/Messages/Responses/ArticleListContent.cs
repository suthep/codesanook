﻿using System.Collections.Generic;
using CodeSanook.MicroBlog.Model;

namespace CodeSanook.MicroBlog.Business.Messages.Responses
{
    public class ArticleListContent
    {
        public IList<Article> ArticleList { get; set; }
        public int NumberOfArticles { get; set; }
    }
}
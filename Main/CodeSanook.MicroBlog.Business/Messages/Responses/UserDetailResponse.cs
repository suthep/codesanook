﻿using System;

namespace CodeSanook.MicroBlog.Business.Messages.Responses
{
   public class UserDetailResponse
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public DateTime RegisterDate { get; set; }

        public bool Activated { get; set; }

        public bool Active { get; set; }

        public string DisplayName { get; set; }

        public string[] Roles { get; set; }
    }
}

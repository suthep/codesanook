using System;

namespace CodeSanook.MicroBlog.Business.Messages.Responses
{

    public class ServiceResponse
    {
        public bool IsSuccess { get; protected set; }
        public string ErrorMessage { get; protected set; }

        public ServiceResponse()
        {
            IsSuccess = true;
        }

        public ServiceResponse(string errorMessage)
        {
            ErrorMessage = errorMessage;
            IsSuccess = false;
        }

        public ServiceResponse(Exception ex)
        {
            ErrorMessage = ex.Message;
            IsSuccess = false;
        }
    }

    public class ServiceResponse<T>:ServiceResponse
    {
        public T Content { get; private set; }

        public ServiceResponse(T content)
        {
            Content = content;
            IsSuccess = true;
        }

        public ServiceResponse(Exception ex)
            : base(ex)
        {

        }


        public ServiceResponse(string errorMessage)
            : base(errorMessage)
        {

        }
    }
}